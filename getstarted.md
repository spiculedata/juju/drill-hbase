# Apache Drill with HBase

## Adding Data To HBase

### Importing SQL data

To import a SQL dataset run the following commands:

```
juju ssh hbase/0
echo "create 'students','account','address'" | hbase shell
echo "create 'clicks','clickinfo','iteminfo'" | hbase shell

cat > testdata.txt
```

paste the following:

```
put 'students','student1','account:name','Alice'
put 'students','student1','address:street','123 Ballmer Av'
put 'students','student1','address:zipcode','12345'
put 'students','student1','address:state','CA'
put 'students','student2','account:name','Bob'
put 'students','student2','address:street','1 Infinite Loop'
put 'students','student2','address:zipcode','12345'
put 'students','student2','address:state','CA'
put 'students','student3','account:name','Frank'
put 'students','student3','address:street','435 Walker Ct'
put 'students','student3','address:zipcode','12345'
put 'students','student3','address:state','CA'
put 'students','student4','account:name','Mary'
put 'students','student4','address:street','56 Southern Pkwy'
put 'students','student4','address:zipcode','12345'
put 'students','student4','address:state','CA'
put 'clicks','click1','clickinfo:studentid','student1'
put 'clicks','click1','clickinfo:url','http://www.google.com'
put 'clicks','click1','clickinfo:time','2014-01-01 12:01:01.0001'
put 'clicks','click1','iteminfo:itemtype','image'
put 'clicks','click1','iteminfo:quantity','1'
put 'clicks','click2','clickinfo:studentid','student1'
put 'clicks','click2','clickinfo:url','http://www.amazon.com'
put 'clicks','click2','clickinfo:time','2014-01-01 01:01:01.0001'
put 'clicks','click2','iteminfo:itemtype','image'
put 'clicks','click2','iteminfo:quantity','1'
put 'clicks','click3','clickinfo:studentid','student2'
put 'clicks','click3','clickinfo:url','http://www.google.com'
put 'clicks','click3','clickinfo:time','2014-01-01 01:02:01.0001'
put 'clicks','click3','iteminfo:itemtype','text'
put 'clicks','click3','iteminfo:quantity','2'
put 'clicks','click4','clickinfo:studentid','student2'
put 'clicks','click4','clickinfo:url','http://www.ask.com'
put 'clicks','click4','clickinfo:time','2013-02-01 12:01:01.0001'
put 'clicks','click4','iteminfo:itemtype','text'
put 'clicks','click4','iteminfo:quantity','5'
put 'clicks','click5','clickinfo:studentid','student2'
put 'clicks','click5','clickinfo:url','http://www.reuters.com'
put 'clicks','click5','clickinfo:time','2013-02-01 12:01:01.0001'
put 'clicks','click5','iteminfo:itemtype','text'
put 'clicks','click5','iteminfo:quantity','100'
put 'clicks','click6','clickinfo:studentid','student3'
put 'clicks','click6','clickinfo:url','http://www.google.com'
put 'clicks','click6','clickinfo:time','2013-02-01 12:01:01.0001'
put 'clicks','click6','iteminfo:itemtype','image'
put 'clicks','click6','iteminfo:quantity','1'
put 'clicks','click7','clickinfo:studentid','student3'
put 'clicks','click7','clickinfo:url','http://www.ask.com'
put 'clicks','click7','clickinfo:time','2013-02-01 12:45:01.0001'
put 'clicks','click7','iteminfo:itemtype','image'
put 'clicks','click7','iteminfo:quantity','10'
put 'clicks','click8','clickinfo:studentid','student4'
put 'clicks','click8','clickinfo:url','http://www.amazon.com'
put 'clicks','click8','clickinfo:time','2013-02-01 22:01:01.0001'
put 'clicks','click8','iteminfo:itemtype','image'
put 'clicks','click8','iteminfo:quantity','1'
put 'clicks','click9','clickinfo:studentid','student4'
put 'clicks','click9','clickinfo:url','http://www.amazon.com'
put 'clicks','click9','clickinfo:time','2013-02-01 22:01:01.0001'
put 'clicks','click9','iteminfo:itemtype','image'
put 'clicks','click9','iteminfo:quantity','10'
```

Press CTL-z then:

```
cat testdata.txt | hbase shell
```

### Querying data

To execute data within Drill, go to the query page and you can run queries similar to:

```
SELECT CONVERT_FROM(row_key, 'UTF8') AS studentid, 
        CONVERT_FROM(students.account.name, 'UTF8') AS name, 
        CONVERT_FROM(students.address.state, 'UTF8') AS state, 
        CONVERT_FROM(students.address.street, 'UTF8') AS street, 
        CONVERT_FROM(students.address.zipcode, 'UTF8') AS zipcode 
FROM students;
```

For more info visit: http://drill.apache.org/docs/querying-hbase/

### Scaling

To scale HBase run the following:

```
juju add-unit hbase
```

To scale Drill run:

```
juju add-unit apache-drill
```

You can then run 

```
juju status
```

or watch the status pane in the GUI to monitor progress.

## Find out more

http://spicule.co.uk
http://drill.apache.org
http://hbase.apache.org